package com.studentApp.StudentManagement.controller;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.studentApp.StudentManagement.entity.Course;
import com.studentApp.StudentManagement.entity.Student;
import com.studentApp.StudentManagement.exception.StudentNotFoundException;
import com.studentApp.StudentManagement.repository.CourseRepository;
import com.studentApp.StudentManagement.repository.StudentRepository;

@RestController
public class StudentController {
	
	@Autowired
	StudentRepository srepo;
	
	@Autowired
	CourseRepository crepo;
	
	
	@GetMapping("/student/{id}")
	public Optional <Student> getStudentById(@PathVariable int id)
	{
		Optional <Student> s = srepo.findById(id);
		if(s.isPresent())
			return s;
		else
			throw new StudentNotFoundException("The student is absent! Enter a valid ID!");
	}

	@GetMapping("/allStudents")
	public List <Student> getAllStudents()
	{
		List <Student> all = srepo.findAll();
		return all;
	}
	
	@GetMapping("/studentsByCourse/{courseId}")
	public List <Student> getStudentsByCourse(@PathVariable("courseId") int courseId)
	{
		List <Student> studentsByCourse = srepo.findByCoursesId(courseId);
		return studentsByCourse;
	}
	
	@GetMapping("/numberOfStudentsByCourse/{courseId}")
	public String getNumberOfStudentsByCourse(@PathVariable("courseId") int courseId)
	{
		List <Student> studentsByCourse = srepo.findByCoursesId(courseId);
		Optional <Course> course = crepo.findById(courseId);
		return ("Number of students enrolled for " + course + "is : " + studentsByCourse.size());
	}
	
	/*Multiple entries of the same record are inserted in student table when one student 
	 * is registered in multiple courses, one by one
	 */
	@Transactional
	@PostMapping("/registerStudent")
	public String registerStudent(@RequestBody Student st, @RequestParam("course_name") String name)
	{
		List <Course> course = crepo.findByName(name);
		st.setCourses(course);
		srepo.save(st);
		return ("student registered succesfully to course : " + name);
	}
	
	@GetMapping("/studentByCoursesName")
	public List <Student> getStudentsByCourseName(@RequestParam("course_name") String name)
	{
		List <Student> st = srepo.findByCoursesName(name);
		return st;
	}
	
	@DeleteMapping("/removeStudent/{id}")
	public String removeStudent(@PathVariable int id)
	{
		srepo.deleteById(id);
		return "student deleted successfully";
	}
}
