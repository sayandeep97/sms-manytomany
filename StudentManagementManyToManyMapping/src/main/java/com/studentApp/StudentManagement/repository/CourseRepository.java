package com.studentApp.StudentManagement.repository;

import java.util.List;

//import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.studentApp.StudentManagement.entity.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {

	List <Course> findByStudentsId(int studentId);
	
	List <Course> findByName(String name);
	
	List <Course> findByStudentsFirstName(String firstName);
}
